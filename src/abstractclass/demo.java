package abstractclass;

public  abstract class demo {
    static int k=20;
    double d=35.25;

    abstract void test();

    void display(){
        System.out.println("Display method");
    }

    static void info(){
        System.out.println("Info Method");
    }

    demo(){
        System.out.println("Constructor");
    }

    static{
        System.out.println("Static block");
    }

    {
        System.out.println("Non static block");
    }


}
