package abstraction;

public interface Account {
    void deposit(double amt);
    void withdraw(double amr);
    void checkBalance();
}
