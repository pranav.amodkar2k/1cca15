package abstraction;

public class LoanAccount implements Account{
    double loanAmount;
    public LoanAccount(double LoanAmount){
        this.loanAmount=loanAmount;
        System.out.println("Loan Account Created");

    }
    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"Rs Debited from your account");
    }

    @Override
    public void withdraw(double amt) {
            loanAmount+=amt;
        System.out.println(amt+"Rs Created to your Account");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active loan account"+loanAmount);
    }
}
