package abstraction;
// step2
public class SavingsAccount implements Account{
    double accountBalance;
    public SavingsAccount(double accountBalance){
        this.accountBalance=accountBalance;
        System.out.println("Savings Account Created ");
    }
    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+" Rs Creeted To your Account");

    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+"Rs Debited from your acoount");
        }
        else {
            System.out.println("Insufficient balance account");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance"+accountBalance);
    }
}
