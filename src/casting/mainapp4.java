package casting;

import javax.jws.soap.SOAPBinding;
import java.util.Scanner;

public class mainapp4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Service Provider");
        System.out.println("1:Airasia\n2:Indigo");
        int choice= sc.nextInt();
        System.out.println("Select Route");
        System.out.println("0:PUNE TO DELHI");
        System.out.println("1:MUMBAI TO CHENNAI");
        System.out.println("2:KOLKATA TO BANGALORE");
        int routechoice=sc.nextInt();
        System.out.println("Enter no of tickets");
        int tickets= sc.nextInt();

        Goibibo g1=null;
        if(choice==1){
            g1=new Airasia();
        }
        else if(choice==2){
            g1=new Indigo();
        }
        g1.bookTicket(tickets,routechoice);

    }
}
